Welcome to the Datathon case.

In order to start developing a solution make a fork of the current project.

This project contains everything you need to develop a solution to the current case study:

1) General information on how to solve your case. Before looking into any files here, please open the wiki section and read everything there.

2) Specific information containing the requirements of the current case. Go to '/case_study'. Make sure you understand the task and if you have questions turn to the mentors of the case.

3) The datasets which you need to solve the case. Got to '/data/'.

